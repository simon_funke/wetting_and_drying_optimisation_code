Introduction
------------

This repository contains the source code and input files to reproduce the
results of the paper

S.W. Funke, et al, Reconstructing wave profiles from inundation data, to appear


Installation instructions
-------------------------

The code runs with FEniCS 2016.2.0 and dolfin-adjoint 2016.2.0

### Installation with docker
1. Install docker
2. Build docker container with
       `docker build -t wd .`
3. Start docker container with
       `docker run -i -t wd`

Software dependencies
---------------------
* FEniCS 2016.1  (www.fenicsproject.org)
* dolfin-adjoint 2016.1  (www.dolfin-adjoint.org)


How to reproduce results
------------------------

* Section 2.4 Verification

    ```bash
    $ cd code/thacker
    ```

    ```bash
    $ make convergence
    ```

* Section 2.5 Volume conservation

    ```bash
    $ cd code/thacker
    ```

    ```bash
    $ make volume_conversation
    ```

* Section 4.1.1 Sinusoidal Dirichlet boundary

    ```bash
    $ cd code/balzano1_alpha=1.8
    ```

    ```bash
    $ make
    ```

    ```bash
    $ cd code/balzano1_alpha=0.43
    ```

    ```bash
    $ make
    ```

* Section 4.1.2 Composed sinusoidal Dirichlet boundary

    ```bash
    $ cd code/balzano1_alpha=0.43_composed_sin
    ```

    ```bash
    $ make
    ```


* Section 4.2 Reconstruction of the Hokkaido-Nansei-Oki tsunami wave profile

    ```bash
    $ cd code/hokkaido-nansei-oki_tsunami_final7_high_res
    ```

    ```bash
    $ make
    ```
