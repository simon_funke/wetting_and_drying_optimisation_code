''' This example implements the shallow water equations with wetting and drying algorithm based on:
  Kaernae et al., A fully implicity wetting-drying method for DG-FEM shallow water models, with an application to the Schledt Estuary, 2011, Comput. Methods Appl. Mech. Engrg.
  '''
from dolfin import *
import numpy

# Global settings
parameters["std_out_all_processes"] = False
dolfin.parameters['form_compiler']['cpp_optimize'] = True
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3 -ffast-math -march=native'
dolfin.parameters['form_compiler']['optimize'] = True
set_log_level(ERROR)

def compute_volume_error(size, T = 24*60*60, dt = 300, output=True):
  # Smoothing value
  alpha = 0.18*size

  mesh = Mesh("mesh"+str(size)+".xml")
  n = FacetNormal(mesh)
  surf_ids = MeshFunction("uint", mesh, "mesh"+str(size)+"_facet_region.xml")
  ds = Measure("ds")[surf_ids]

  # The functions play following role:
  # eta: free surface elevation versus a reference level
  # h: static bathymetry
  # H: eta + h
  # \tilde h: h + f(H), where f is an approximation to the function: x -> max(|-x|, 0)
  # \tilde H: H = eta + \tilde h > 0!
  V = VectorFunctionSpace(mesh, "DG", 1)
  Vcomp = FunctionSpace(mesh, "DG", 1)
  P = FunctionSpace(mesh, "CG", 2)
  Z = MixedFunctionSpace([V, P])

  z_new = Function(Z)
  (u_new, eta_new) = split(z_new)
  z_1 = Function(Z)
  (u_1, eta_1) = split(z_1)
  z_2 = Function(Z)
  (u_2, eta_2) = split(z_2)
  z_new = Function(Z)
  (u_new, eta_new) = split(z_new)
  z_old = Function(Z)
  (u_old, eta_old) = split(z_old)
  z_test = TestFunction(Z)
  (u_test, eta_test) = split(z_test)
  z_analytical = Function(Z)
  (u_analytical, eta_analytical) = split(z_analytical)

  def wd_f(H, alpha):
    return 0.5*(sqrt(H**2+alpha**2)-H)

  def norm_approx(u, alpha):
    # A smooth approximation to ||u||
    return sqrt(inner(u, u)+alpha**2)

  h0 = 50.
  L = 430620.
  X = SpatialCoordinate(mesh)
  h_static = h0*(1.-(X[0]**2+X[1]**2)/L**2)

  def H_tilde(eta):
    return h_static + wd_f(h_static + eta, alpha) + eta

  t = 0.
  g = 9.81

  # The analytical solution
  eta0 = 2.
  A = ((h0+eta0)**2 - h0**2) / ((h0+eta0)**2 + h0**2)
  omega = sqrt(8*g*h0/L**2)
  analytical_eta = "h0*(sqrt(1-A*A)/(1-A*cos(omega*t)) - 1 - (x[0]*x[0] + x[1]*x[1])/(L*L) * ((1-A*A)/(pow(1-A*cos(omega*t), 2)) - 1))"
  analytical_u_x = "0.5/(1-A*cos(omega*t)) * (omega*x[0]*A*sin(omega*t))"
  analytical_u_y = "0.5/(1-A*cos(omega*t)) * (omega*x[1]*A*sin(omega*t))"
  analytic_sol = Expression((analytical_u_x, analytical_u_y, analytical_eta), h0 = h0, L = L, A = A, omega = omega, t = t)
  z_analytical.interpolate(analytic_sol)
  z_new.assign(z_analytical)
  z_old.assign(z_analytical)

  # DIRK22 Runge Kutta method
  a11 = (2.-sqrt(2.))/2.
  a21 = 1.-(2.-sqrt(2.))/2.
  a22 = (2.-sqrt(2.))/2.
  b1 = 1.-(2.-sqrt(2.))/2.
  b2 = (2.-sqrt(2.))/2.

  def continuity(a, u, eta, eta_test):
    return (- a * inner(grad(eta_test), H_tilde(eta)*u)*dx
           )

  def momentum(a, u, u_test, eta):
    u_down = (dot(u, n)-norm_approx(dot(u, n), alpha))/2.0
    return (#a * inner(grad(u)*u, u_test)*dx
            a * dot(dot(u, nabla_grad(u)), u_test)*dx
            - a * dot(u_down('+')*jump(u), u_test('+'))*dS
            + a * dot(u_down('-')*jump(u), u_test('-'))*dS
            + a * g*inner(grad(eta), u_test)*dx
           )

  F1 = (inner((u_1 - u_old)/dt, u_test)*dx
        + momentum(a11, u_1, u_test, eta_1)
        + inner((H_tilde(eta_1) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(a11, u_1, eta_1, eta_test)
       )

  F2 = (inner((u_2 - u_old)/dt, u_test)*dx
        + momentum(a21, u_1, u_test, eta_1)
        + momentum(a22, u_2, u_test, eta_2)
        + inner((H_tilde(eta_2) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(a21, u_1, eta_1, eta_test)
        + continuity(a22, u_2, eta_2, eta_test)
       )

  F3 = (inner((u_new - u_old)/dt, u_test)*dx
        + momentum(b1, u_1, u_test, eta_1)
        + momentum(b2, u_2, u_test, eta_2)
        + inner((H_tilde(eta_new) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(b1, u_1, eta_1, eta_test)
        + continuity(b2, u_2, eta_2, eta_test)
       )

  volume = [assemble(H_tilde(eta_new)*dx)]

  while t < T:
    # Use the default tolerance of 1e-9
    #solver_parameters = {}
    #solver_parameters["newton_solver"] = {}
    #solver_parameters["newton_solver"]["relative_tolerance"] = 1e-09 # -9 is the default value
    solve(F1 == 0, z_1)#, solver_parameters = solver_parameters)
    solve(F2 == 0, z_2)#, solver_parameters = solver_parameters)
    solve(F3 == 0, z_new)#, solver_parameters = solver_parameters)
    z_old.assign(z_new)
    t += dt

    volume.append(assemble(H_tilde(eta_new)*dx))

  return max(abs(numpy.array(volume)-volume[0])/volume[0])

if __name__ == "__main__":
  errors = {}
  for size in [40]:
    errors[size] = {}
    for qd in [4, 20 ,121]:
      errors[size][qd] = {}
      parameters["form_compiler"]["quadrature_degree"] = qd
      info(parameters, True)
      timer = Timer("Running model with size=" + str(size) + " and qd=" + str(qd));
      timer.start()
      errors[size][qd]["error"] = compute_volume_error(size)
      errors[size][qd]["runtime"] = timer.stop()
      if dolfin.MPI.rank(dolfin.mpi_comm_world()) == 0:
        print "Relative error for mesh ", size, " and quadrature degree ", qd, " : ", errors[size][qd]["error"], " (runtime: ", errors[size][qd]["runtime"], ")"
  if dolfin.MPI.rank(dolfin.mpi_comm_world()) == 0:
    print errors
