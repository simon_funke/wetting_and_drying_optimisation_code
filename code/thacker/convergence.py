from thacker_dg import compute_error
from dolfin import MPI, mpi_comm_world
import dolfin_adjoint
from matplotlib import rc
import matplotlib.pyplot as plt

def save_convergence_plot(errors, element_sizes, title, legend, order, offset = 1.1, show_title=True):
    ''' Creates a convergence plot '''
    if MPI.rank(mpi_comm_world()) != 0:
        return

    # Plot the errors
    scaling = 0.7
    rc('text', usetex=True)
    plt.figure(1, figsize=(scaling*7,scaling*4))

    plt.xlabel('Element size')
    plt.ylabel('Error')
    if show_title:
        plt.title(title)

    plt.loglog(element_sizes, errors, 'gx', label = legend, color = 'black')
    # Construct an error plot with the expected order
    c = errors[-1]/element_sizes[-1]**order*offset
    expected_errors = [c*element_size**order for element_size in element_sizes]
    plt.loglog(element_sizes, expected_errors, label = str(order) + ' order', color = 'black')

    plt.legend(loc=4)
    plt.axis([9, 100, min(errors)*0.7, max(errors)*1.3])
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.xlabel("Element size [km]")
    plt.ylabel(r"Surface elevation error $\mathcal E$")

    filename = '_'.join(title.split()).lower()
    print 'Saving as: ', filename
    plt.savefig(filename + '.png')
    plt.savefig(filename + '.pdf')
    plt.close()


if __name__ == '__main__':
    T = 24*60*60
    errors = []
    element_sizes = [40, 30, 20, 10]
    for s in element_sizes:
      errors.append(compute_error(size=s, T = T, output = False, dt = 300))
      if MPI.rank(mpi_comm_world()) != 0:
        print "*** Error for size %i *** : %f" % (s, errors[-1])
    print "Error summary: ", errors
    print "Element sizes: ", element_sizes
    save_convergence_plot(errors, element_sizes, "Rate of convergence for the surface elevation", "Numerical error", order = 1.5, show_title = False)
    print "Convergence orders: ", dolfin_adjoint.convergence_order(errors[0:1] + errors[2:])
