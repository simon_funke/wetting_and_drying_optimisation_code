''' This example implements the shallow water equations with wetting and drying algorithm based on:
  Kaernae et al., A fully implicity wetting-drying method for DG-FEM shallow water models, with an application to the Schledt Estuary, 2011, Comput. Methods Appl. Mech. Engrg.
  '''
from dolfin import *
from detectors import *

# Global settings
parameters["std_out_all_processes"] = False
dolfin.parameters['form_compiler']['cpp_optimize'] = True
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3 -ffast-math -march=native'
dolfin.parameters['form_compiler']['optimize'] = True
parameters["form_compiler"]["quadrature_degree"] = 20
set_log_level(ERROR)

def compute_error(size, T = 24*60*60, dt = 300, output=True):
  # Smoothing value
  alpha = 0.18*size

  mesh = Mesh("mesh"+str(size)+".xml")
  n = FacetNormal(mesh)
  surf_ids = MeshFunction("size_t", mesh, "mesh"+str(size)+"_facet_region.xml")
  ds = Measure("ds")(subdomain_data=surf_ids)

  # The functions play following role:
  # eta: free surface elevation versus a reference level
  # h: static bathymetry
  # H: eta + h
  # \tilde h: h + f(H), where f is an approximation to the function: x -> max(|-x|, 0)
  # \tilde H: H = eta + \tilde h > 0!
  dg1 = VectorElement("DG", triangle, 1)
  cg2 = FiniteElement("CG", triangle, 2)
  dg1cg2 = MixedElement([dg1, cg2])

  Vcomp = FunctionSpace(mesh, "DG", 1)
  P = FunctionSpace(mesh, cg2)
  Pdg = FunctionSpace(mesh, "DG", 2)
  Z = FunctionSpace(mesh, dg1cg2)

  if output:
    u_pvd = File("results_dg/u.pvd")
    eta_pvd = File("results_dg/eta.pvd")
    h_static_pvd = File("results_dg/h_static.pvd")
    h_tilde_pvd = File("results_dg/h_tilde.pvd")
    surface_elevation_pvd = File("results_dg/surface_elevation.pvd")
    wet_dry_pvd = File("results_dg/wet_dry.pvd")
    u_analytical_pvd = File("results_dg/u_analytical.pvd")
    eta_analytical_pvd = File("results_dg/eta_analytical.pvd")

  def store(z, h_static, h_tilde, z_analytical, t):
    t=float(t)
    (u, eta) = z.split(deepcopy=True)
    u_pvd << (u, t)
    eta_pvd << (eta, t)
    h_static_pvd << (project(h_static, P), t)
    h_tilde_pvd << (project(h_tilde, P), t)
    surface_elevation_pvd << (project(conditional(gt(eta, -h_static), eta, -h_static), Pdg), t)
    wet_dry_pvd << (project(conditional(gt(eta, -h_static), 1., 0.), Pdg), t)
    (u_analytical, eta_analytical) = z_analytical.split(deepcopy=True)
    u_analytical_pvd << (u_analytical, t)
    eta_analytical_pvd << (eta_analytical, t)

  z_new = Function(Z)
  (u_new, eta_new) = split(z_new)
  z_1 = Function(Z)
  (u_1, eta_1) = split(z_1)
  z_2 = Function(Z)
  (u_2, eta_2) = split(z_2)
  z_new = Function(Z)
  (u_new, eta_new) = split(z_new)
  z_old = Function(Z)
  (u_old, eta_old) = split(z_old)
  z_test = TestFunction(Z)
  (u_test, eta_test) = split(z_test)
  z_analytical = Function(Z)
  (u_analytical, eta_analytical) = split(z_analytical)

  def wd_f(H, alpha):
    return 0.5*(sqrt(H**2+alpha**2)-H)

  def norm_approx(u, alpha):
    # A smooth approximation to ||u||
    return sqrt(inner(u, u)+alpha**2)

  h0 = 50.
  L = 430620.
  X = SpatialCoordinate(mesh)
  h_static = h0*(1.-(X[0]**2+X[1]**2)/L**2)

  def H_tilde(eta):
    return h_static + wd_f(h_static + eta, alpha) + eta

  t = 0.
  g = 9.81

  # The analytical solution
  eta0 = 2.
  A = ((h0+eta0)**2 - h0**2) / ((h0+eta0)**2 + h0**2)
  omega = sqrt(8*g*h0/L**2)
  analytical_eta = "h0*(sqrt(1-A*A)/(1-A*cos(omega*t)) - 1 - (x[0]*x[0] + x[1]*x[1])/(L*L) * ((1-A*A)/(pow(1-A*cos(omega*t), 2)) - 1))"
  analytical_u_x = "0.5/(1-A*cos(omega*t)) * (omega*x[0]*A*sin(omega*t))"
  analytical_u_y = "0.5/(1-A*cos(omega*t)) * (omega*x[1]*A*sin(omega*t))"
  analytic_sol = Expression((analytical_u_x, analytical_u_y, analytical_eta), h0 = h0, L = L, A = A, omega = omega, t = t, degree=4)
  z_analytical.interpolate(analytic_sol)
  z_new.assign(z_analytical)
  z_old.assign(z_analytical)

  # DIRK22 Runge Kutta method
  a11 = (2.-sqrt(2.))/2.
  a21 = 1.-(2.-sqrt(2.))/2.
  a22 = (2.-sqrt(2.))/2.
  b1 = 1.-(2.-sqrt(2.))/2.
  b2 = (2.-sqrt(2.))/2.

  def continuity(a, u, eta, eta_test):
    return (- a * inner(grad(eta_test), H_tilde(eta)*u)*dx
           )

  def momentum(a, u, u_test, eta):
    u_down = (dot(u, n)-norm_approx(dot(u, n), alpha))/2.0
    return (#a * inner(grad(u)*u, u_test)*dx
            a * dot(dot(u, nabla_grad(u)), u_test)*dx
            - a * dot(u_down('+')*jump(u), u_test('+'))*dS
            + a * dot(u_down('-')*jump(u), u_test('-'))*dS
            + a * g*inner(grad(eta), u_test)*dx
           )

  F1 = (inner((u_1 - u_old)/dt, u_test)*dx
        + momentum(a11, u_1, u_test, eta_1)
        + inner((H_tilde(eta_1) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(a11, u_1, eta_1, eta_test)
       )

  F2 = (inner((u_2 - u_old)/dt, u_test)*dx
        + momentum(a21, u_1, u_test, eta_1)
        + momentum(a22, u_2, u_test, eta_2)
        + inner((H_tilde(eta_2) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(a21, u_1, eta_1, eta_test)
        + continuity(a22, u_2, eta_2, eta_test)
       )

  F3 = (inner((u_new - u_old)/dt, u_test)*dx
        + momentum(b1, u_1, u_test, eta_1)
        + momentum(b2, u_2, u_test, eta_2)
        + inner((H_tilde(eta_new) - H_tilde(eta_old))/dt, eta_test)*dx
        + continuity(b1, u_1, eta_1, eta_test)
        + continuity(b2, u_2, eta_2, eta_test)
       )

  if output:
    # Define detectors
    detectors = [DetectorSet("Velocity at origin", [Detector(u_new[0], (0., 0.), "Velocity x-direction", Vcomp),
                                                    Detector(u_analytical[0], (0., 0.), "Analytical velocity x-direction", Vcomp),
                 ]),
                 DetectorSet("Eta at origin", [Detector(eta_new, (0., 0.), "Eta", P),
                                               Detector(eta_analytical, (0., 0.), "Analytical eta", P),
                 ]),
                 DetectorSet("Velocity at x=" + str(L/2/1000) + "m", [Detector(u_new[0], (L/2, 0.), "Velocity x-direction", Vcomp),
                                                    Detector(u_analytical[0], (L/2, 0.), "Analytical velocity x-direction", Vcomp),
                 ]),
                 DetectorSet("Eta at x=" + str(L/2/1000) + "m", [Detector(eta_new, (L/2, 0.), "Eta", P),
                                                      Detector(eta_analytical, (L/2, 0), "Analytical eta", P),
                 ]),
                ]
    [d.update(t/60/60) for d in detectors]

    store(z_new, h_static, H_tilde(eta_new) - eta_new, z_analytical, t)

  error = errornorm(project(H_tilde(eta_new)-h_static, Pdg),
          project(conditional(gt(analytic_sol[2], -h_static), analytic_sol[2],
              -h_static), Pdg), degree_rise=2)

  while t < T:
    if dolfin.MPI.rank(dolfin.mpi_comm_world()) == 0:
      print "Computing time t=%f h" % (float(t)/60/60)

    solve(F1 == 0, z_1)
    solve(F2 == 0, z_2)
    solve(F3 == 0, z_new)
    z_old.assign(z_new)
    t += dt
    analytic_sol.t = t
    z_analytical.interpolate(analytic_sol)

    if output:
      [d.update(t/60/60) for d in detectors]
      store(z_new, h_static, H_tilde(eta_new) - eta_new, z_analytical, t)

    error_t = errornorm(project(H_tilde(eta_new)-h_static, Pdg),
            project(conditional(gt(analytic_sol[2], -h_static), analytic_sol[2],
                -h_static), Pdg), degree_rise=2)
    error += error_t

  if output:
    [d.save_plot() for d in detectors]
  return error

if __name__ == "__main__":
  compute_error(size=10)
