import matplotlib.pyplot as plt
from dolfin import MPI, project, mpi_comm_world

class DetectorSet(list):
  ''' A class that can hold a set of Detectors objects and operate on them '''
  def __init__(self, title, *args, **kwargs):
    self.title = title
    super(DetectorSet, self).__init__(*args, **kwargs)

  def update(self, time):
    [d.update(time) for d in self]

  def save_plot(self):
    if MPI.rank(mpi_comm_world()) == 0:
      plt.clf()
      plt.title(self.title)
      [d.plot() for d in self]
      plt.legend()
      plt.savefig('_'.join(self.title.lower().split()) + ".png")
      plt.clf()

class Detector(object):
  ''' A class that stores the function values of a series of timesteps '''
  def __init__(self, function, point, label, functionspace = None):
    self.label = label
    self.function = function
    self.point = point
    self.values = []
    self.times = []
    self.functionspace = functionspace

  def update(self, time):
    self.times.append(time)
    try:
      value = self.function(self.point)
    # Catch RuntimeError that is raised on CPUs that do not own the evaluation cell
    except RuntimeError:
      value = -1000000.
      pass
    value = MPI.max(mpi_comm_world(), value)

    self.values.append(value)


  def plot(self):
    plt.plot(self.times, self.values, label=self.label)
