// Basin dimension
L = 495200;
element_size = 15000;

Point(1) = {0, 0, 0, element_size};
Point(2) = {L, 0, 0, element_size};
Point(3) = {-L, 0, 0, element_size};
Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 2};
Line Loop(3) = {1, 2};
Plane Surface(4) = {3};
Physical Line(1) = {1, 2};
Physical Surface(7) = {4};
