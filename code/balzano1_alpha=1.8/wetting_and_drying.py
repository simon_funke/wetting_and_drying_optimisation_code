''' This example implements the shallow water equations with wetting and drying algorithm based on:
  Kaernae et al., A fully implicity wetting-drying method for DG-FEM shallow water models, with an application to the Schledt Estuary, 2011, Comput. Methods Appl. Mech. Engrg.
  '''
from dolfin import *
from dolfin_adjoint import *
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy

# Global settings
parameters["std_out_all_processes"] = False
dolfin.parameters['form_compiler']['cpp_optimize'] = True
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3 -ffast-math -march=native'
dolfin.parameters['form_compiler']['optimize'] = True
#dolfin.parameters["optimization"]["test_gradient"] = True
parameters["form_compiler"]["quadrature_degree"] = 20
set_log_level(ERROR)
# From mesh.geo
basin_x = 13800.
basin_x_total = 1.5*basin_x
basin_y = 1200.

alpha = Constant(1.8)
period = 12.*60*60
offset = 6.*60*60
elevation = 1.0

def compute_solution(T = 24*60*60, dlt = 600., output=True):
  ########################## Step 0: Define the equations and do the setup work #####################

  mesh = Mesh("mesh.xml")
  n = FacetNormal(mesh)
  surf_ids = MeshFunction("size_t", mesh, "mesh_facet_region.xml")
  ds = Measure("ds")(subdomain_data=surf_ids)

  # The functions play following role:
  # eta: free surface elevation versus a reference level
  # h: static bathymetry
  # H: eta + h
  # \tilde h: h + f(H), where f is an approximation to the function: x -> max(|-x|, 0)
  # \tilde H: H = eta + \tilde h > 0!
  dg1 = VectorElement("DG", triangle, 1)
  cg2 = FiniteElement("CG", triangle, 2)
  dg1cg2 = MixedElement([dg1, cg2])

  P = FunctionSpace(mesh, cg2)
  Pdg = FunctionSpace(mesh, "DG", 6)
  R = FunctionSpace(mesh, "R", 0)
  Z = FunctionSpace(mesh, dg1cg2)

  if output:
    u_pvd = File("results_dg/u.pvd")
    eta_pvd = File("results_dg/eta.pvd")
    h_static_pvd = File("results_dg/h_static.pvd")
    h_tilde_pvd = File("results_dg/h_tilde.pvd")
    surface_elevation_pvd = File("results_dg/surface_elevation.pvd")
    wet_dry_pvd = File("results_dg/wet_dry.pvd")
    smooth_wet_dry_pvd = File("results_dg/wd_smooth.pvd")
    obs_pvd = File("results_dg/observations.pvd")

  def store(z, h_static, h_tilde, t):
    t=float(t)
    (u, eta) = z.split(deepcopy=True)
    u_pvd << (u, t)
    eta_pvd << (eta, t)
    h_static_pvd << (project(h_static, P, annotate = False), t)
    h_tilde_pvd << (project(h_tilde, P, annotate = False), t)
    surface_elevation_pvd << (project(conditional(gt(eta, -h_static), eta, -h_static), Pdg, annotate = False), t)
    wet_dry_pvd << (project(conditional(gt(eta, -h_static), 1., 0.), Pdg, annotate = False), t)
    smooth_wet_dry_pvd << (project(heavyside_approx(- eta - h_static, alpha), Pdg, annotate = False), t)

  z_1 = Function(Z, name = "z1")
  (u_1, eta_1) = split(z_1)
  z_2 = Function(Z, name = "z2")
  (u_2, eta_2) = split(z_2)
  z_new = Function(Z, name = "z_new")
  (u_new, eta_new) = split(z_new)
  z_old = Function(Z, name = "z_old")
  (u_old, eta_old) = split(z_old)
  z_test = TestFunction(Z)
  (u_test, eta_test) = split(z_test)
  wd_obs = Function(Pdg, name = "wd_obs")

  def wd_f(H, alpha):
    # An smooth approximation to max(-H, 0)
    # f(H)
    # \
    #  \
    #   \
    #    - ------
    #    0      H
    return 0.5*(sqrt(H**2+alpha**2)-H)

  def heavyside_approx(H, alpha):
    # A smooth approximation to the heaviside function
    return 0.5*(H/(sqrt(H**2+alpha**2)))+0.5

  def norm_approx(u, alpha):
    # A smooth approximation to ||u||
    return sqrt(inner(u, u)+alpha**2)

  max_depth = 5.
  X = SpatialCoordinate(mesh)
  h_static = (1.-X[0]/basin_x)*max_depth

  def H_tilde(eta):
    return h_static + wd_f(h_static + eta, alpha) + eta

  t = 0.
  g = 9.81
  c0 = 0.025
  eta_bc = Expression(("elevation*(-cos(2*pi*t/period) + 1)/2"), elevation =
          elevation, t = t, period = period, degree=3)

  # DIRK22 Runge Kutta method
  a11 = (2.-sqrt(2.))/2.
  a21 = 1.-(2.-sqrt(2.))/2.
  a22 = (2.-sqrt(2.))/2.
  b1 = 1.-(2.-sqrt(2.))/2.
  b2 = (2.-sqrt(2.))/2.
  c1 = (2.-sqrt(2.))/2.
  c2 = 1.

  def continuity(a, u, eta, eta_test):
    return (- a * inner(grad(eta_test), H_tilde(eta)*u)*dx
            + a * eta_test * H_tilde(eta) * inner(u, n)*ds(1)
           )

  def momentum(a, u, u_test, eta):
    u_down = (dot(u, n)-norm_approx(dot(u, n), alpha))/2.0
    return (#a * inner(grad(u)*u, u_test)*dx
            a * dot(dot(u, nabla_grad(u)), u_test)*dx
            - a * dot(u_down('+')*jump(u), u_test('+'))*dS
            + a * dot(u_down('-')*jump(u), u_test('-'))*dS
            + a * g*inner(grad(eta), u_test)*dx
            - a * (eta-eta_bc) * g * inner(u_test, n)*ds(1)
            #+ a * g * c0**2 * inner(sqrt(inner(u,u)) * u, u_test)*(H_tilde(eta)**(-4./3))*dx
            + a * g * c0**2 * norm_approx(u, alpha) * inner(u, u_test)*(H_tilde(eta)**(-4./3))*dx
           )

  F1 = (inner((u_1 - u_old)/dlt, u_test)*dx
        + momentum(a11, u_1, u_test, eta_1)
        + inner((H_tilde(eta_1) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(a11, u_1, eta_1, eta_test)
       )

  F2 = (inner((u_2 - u_old)/dlt, u_test)*dx
        + momentum(a21, u_1, u_test, eta_1)
        + momentum(a22, u_2, u_test, eta_2)
        + inner((H_tilde(eta_2) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(a21, u_1, eta_1, eta_test)
        + continuity(a22, u_2, eta_2, eta_test)
       )

  F3 = (inner((u_new - u_old)/dlt, u_test)*dx
        + momentum(b1, u_1, u_test, eta_1)
        + momentum(b2, u_2, u_test, eta_2)
        + inner((H_tilde(eta_new) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(b1, u_1, eta_1, eta_test)
        + continuity(b2, u_2, eta_2, eta_test)
       )

  ########################## Step 1: Run the forward model with the optimal parameters and record the wetting and drying front #####################
  if output:
    store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
  adjointer.time.start(t)

  # The following arrays will contain the boundary functions of all timesteps and the upper and lower bounds for the control problem
  # All right, so in order to make this work we need some control fields
  # 1) First of all we create a list of functions each of which describes the boundary value
  #    at a certain timestep (we can not reuse one function since timestep boundary value is independently controlled)
  eta_bc_proj = [project(eta_bc, R)]
  # Keep also a copy of these optimal controls for later comparison
  eta_bc_proj_opt = [project(eta_bc, R)]
  # Record the initial wetting and drying front
  wd_obs_arr = [project(heavyside_approx(- eta_new - h_static, alpha), Pdg, annotate = False)]
  wd_obs = project(wd_obs_arr[-1], Pdg)
  # Record the simulated wetting and drying front
  wd = project(heavyside_approx(- eta_new - h_static, alpha), Pdg)
  # Store the time values and the wet/dry interface position for later plotting
  times = [t]

  while t < T:
    if MPI.rank(mpi_comm_world()) == 0:
      print "Computing time t=%f h" % (float(t)/60/60)
    # Fix the boundary control in the offset hours
    if t >= offset and t < T - offset:
      eta_bc.t = t + dlt - offset
    else:
      eta_bc.t = 0.
    eta_bc_proj.append(project(eta_bc, R))
    eta_bc_proj_opt.append(project(eta_bc, R))

    # Update the boundary values and solve the first Runge-Kutta step
    solve(replace(F1, {eta_bc: (1.-c1)*eta_bc_proj[-2] + c1*eta_bc_proj[-1]}) == 0, z_1)

    # Update the boundary values and solve the second Runge-Kutta step
    solve(replace(F2, {eta_bc: eta_bc_proj[-1]}) == 0, z_2)

    # Save the optimal control values and solve the final Runge-Kutta step
    solve(replace(F3, {eta_bc: eta_bc_proj[-1]}) == 0, z_new)
    z_old.assign(z_new)

    # Record the wetting and drying front
    wd_obs_arr.append(project(heavyside_approx(- eta_new - h_static, alpha), Pdg, annotate = False))
    obs_pvd << (wd_obs_arr[-1], t)
    wd_obs.assign(wd_obs_arr[-1])
    # Record the simulated wetting and drying front
    wd_tmp = project(heavyside_approx(- eta_new - h_static, alpha), Pdg)
    wd.assign(wd_tmp)

    t += dlt
    times.append(t)
    if output:
      store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
    adj_inc_timestep(time=t, finished = t>=T)

  ########################## Step 2: Define the functional using the recorded wetting and drying front  #####################
  form = 0.5*inner(wd - wd_obs, wd - wd_obs)*dx*dt
  scale = Constant(1)
  J = Functional(scale*form)

  def observation_plot(times, wd_obs_arr):
    ''' Plots the observations '''
    # Convert units to h and km
    nb_eval = 20 # Number of evaluations along the slope
    T = [[time/60/60]*nb_eval for time in times]
    X = [numpy.linspace(0, basin_x_total/1000, nb_eval) for t in times]
    Z = [[wd((x*1000, 0)) for x in X[0]] for wd in wd_obs_arr]

    scaling = 0.7
    rc('text', usetex = True)
    plt.figure(1, figsize = (scaling*7., scaling*4.))
    plt.gcf().subplots_adjust(bottom=0.15)

    cset1 = plt.contourf(T, X, Z, 20, cmap = plt.cm.get_cmap('binary'))
    plt.clim(-0.0, 1.2)
    cset2 = plt.contour(T, X, Z, 20, cmap = plt.cm.get_cmap('binary'))
    plt.clim(-0.0, 1.2)
    cset3 = plt.contour(T, X, Z, 1, colors = "black", linestyles = "dotted", linewidths = 5.0, levels = [0.5])
    cb = plt.colorbar(cset1, ticks = numpy.linspace(0, 1, 6))
    cb.set_label("$\mathcal H(\eta -h)$")
    plt.ylim(min(X[0]), max(X[0]))

    plt.xlabel("Time [h]")
    plt.ylabel("x [km]")
    if MPI.rank(mpi_comm_world()) == 0:
      print "Saving wet/dry interface to plot_wd_interface.pdf"
    plt.savefig("plot_wd_interface.pdf")
    plt.close()

  def control_plot(times, controls, filename, ylabel = r"$\eta_D$ [m]", subtract_optimal_controls = False):
    ''' Saves a plot of the controls '''
    endtime = times[-1]/60/60
    times = [time/60/60 for time in times[:-int(2*60*60/dlt)]]
    if subtract_optimal_controls:
      Y = [control(0,0)-opt_control(0,0) for control, opt_control in zip(controls, eta_bc_proj_opt[:-int(2*60*60/dlt)])]
    else:
      Y = [control(0,0) for control in controls]

    scaling = 0.7
    rc('text', usetex = True)
    plt.figure(1, figsize = (scaling*7., scaling*4.))
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.3)
    plt.plot(times[:len(Y)], Y, color = 'black')
    if not subtract_optimal_controls:
      plt.axis([0, endtime, -0.1, elevation*1.2])
    else:
      plt.xlim([0, endtime])
    #plt.xticks(numpy.arange(0, times[-1]+1, 5))
    #plt.yticks(numpy.arange(14, basin_x_total/1000, 2))
    plt.ylabel(ylabel)
    plt.xlabel(r"Time [h]")
    if MPI.rank(mpi_comm_world()) == 0:
      print "Saving control to ", filename
    plt.savefig(filename)
    plt.close()

  # Save a plot of the optimal control values
  control_plot(times, eta_bc_proj_opt[:-int(2*60*60/dlt)], "controls/controls_optimal.pdf", ylabel = r"$\eta_D^{\textrm{exact}}$ [m]")
  observation_plot(times, wd_obs_arr)

  # And create a callback that saves the control values at every optimisation iteration
  eval_counter = [0]
  def eval_callback(value):
    eval_counter[0] += 1
    control_plot(times, value, "controls/controls_"+str(eval_counter[0])+".pdf")
    control_plot(times, value, "controls/controls_final.pdf")
    control_plot(times, value, "controls/controls_errors_"+str(eval_counter[0])+".pdf", subtract_optimal_controls=True, ylabel=r"$\eta_D - \eta_D^{\textrm{exact}}$ [m]")
    control_plot(times, value, "controls/controls_errors_final.pdf", subtract_optimal_controls=True, ylabel = r"$\eta_D - \eta_D^{\textrm{exact}}$ [m]")
    max_err = max([abs(control(0,0)-opt_control(0,0)) for control, opt_control in zip(value, eta_bc_proj_opt[:-int(2*60*60/dlt)])])
    print "Maximal control value error: ", max_err, "m"
    if max_err < 0.001:
      print "********************************** Control is 0.001m close to the solution found after %i iterations ************************" % eval_counter[0]

  ########################## Step 3: Forget the optimal parameters and recover them using the optimisation  #####################
  for p in eta_bc_proj[:-int(2*60*60/dlt)]:
    p.vector()[:] = 0.

  ctrl_values = eta_bc_proj[:-int(2*60*60/dlt)]
  controls = [Control(p) for p in ctrl_values]
  rf = ReducedFunctional(J, controls, eval_cb_pre=eval_callback)
  scale.assign(1./rf(ctrl_values)) # Normalize functional
  minimize(rf, options={'gtol':  1e-200, 'ftol': 1e-9, 'maxfun': 1000})

  ########################## Step 4: Now compare how well we reconstructed the controls  #####################
  errors = [assemble(inner(p_opt-p, p_opt-p)*ds(1)) for (p, p_opt) in zip(eta_bc_proj_opt, eta_bc_proj)]
  for (timestep, error) in zip(range(len(errors)), errors):
    print "Timestep: %i   Error: %f"% (timestep, error)
  print "Total error: ", sum(errors)

if __name__ == "__main__":
  compute_solution(T=period + 2*offset)
