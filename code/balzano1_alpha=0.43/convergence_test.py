''' This example implements the shallow water equations with wetting and drying algorithm based on:
  Kaernae et al., A fully implicity wetting-drying method for DG-FEM shallow water models, with an application to the Schledt Estuary, 2011, Comput. Methods Appl. Mech. Engrg.
  '''
from dolfin import *
from dolfin_adjoint import *

# Global settings
parameters["std_out_all_processes"] = False
dolfin.parameters['form_compiler']['cpp_optimize'] = True
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
dolfin.parameters['form_compiler']['optimize'] = True 
dolfin.parameters["optimisation"]["test_gradient"] = True
parameters["form_compiler"]["quadrature_degree"] = 20
set_log_level(ERROR)
basin_x = 13800.

def compute_solution(T = 24*60*60, dlt = 600., output=True):
  ########################## Step 0: Define the equations and do the setup work #####################
  # Smoothing value 
  alpha = 0.3 

  mesh = Mesh("mesh.xml")
  n = FacetNormal(mesh)
  surf_ids = MeshFunction("uint", mesh, "mesh_facet_region.xml")
  ds = Measure("ds")[surf_ids]

  # The functions play following role:
  # eta: free surface elevation versus a reference level
  # h: static bathymetry
  # H: eta + h 
  # \tilde h: h + f(H), where f is an approximation to the function: x -> max(|-x|, 0)
  # \tilde H: H = eta + \tilde h > 0!
  V = VectorFunctionSpace(mesh, "DG", 1)
  Vcomp = FunctionSpace(mesh, "DG", 1) 
  P = FunctionSpace(mesh, "CG", 2)
  Pdg = FunctionSpace(mesh, "DG", 4)
  R = FunctionSpace(mesh, "R", 0)
  Z = MixedFunctionSpace([V, P])

  if output:
    u_pvd = File("results_dg/u.pvd")
    eta_pvd = File("results_dg/eta.pvd")
    h_static_pvd = File("results_dg/h_static.pvd")
    h_tilde_pvd = File("results_dg/h_tilde.pvd")
    surface_elevation_pvd = File("results_dg/surface_elevation.pvd")
    wet_dry_pvd = File("results_dg/wet_dry.pvd")
    smooth_wet_dry_pvd = File("results_dg/wd_smooth.pvd")

  def store(z, h_static, h_tilde, t):
    t=float(t)
    (u, eta) = z.split(deepcopy=True)
    u_pvd << (u, t)
    eta_pvd << (eta, t)
    h_static_pvd << (project(h_static, P, annotate = False), t)
    h_tilde_pvd << (project(h_tilde, P, annotate = False), t)
    surface_elevation_pvd << (project(conditional(gt(eta, -h_static), eta, -h_static), Pdg, annotate = False), t)
    wet_dry_pvd << (project(conditional(gt(eta, -h_static), 1., 0.), Pdg, annotate = False), t)
    smooth_wet_dry_pvd << (project(heavyside_approx(eta - h_static, alpha), Pdg, annotate = False), t)

  z_1 = Function(Z, name = "z1")
  (u_1, eta_1) = split(z_1)
  z_2 = Function(Z, name = "z2")
  (u_2, eta_2) = split(z_2)
  z_new = Function(Z, name = "z_new")
  (u_new, eta_new) = split(z_new)
  z_old = Function(Z, name = "z_old")
  (u_old, eta_old) = split(z_old)
  z_test = TestFunction(Z)
  (u_test, eta_test) = split(z_test)
  wd_obs = Function(Pdg, name = "wd_obs")

  def wd_f(H, alpha):
    # An smooth approximation to max(-H, 0)
    # f(H)
    # \
    #  \
    #   \
    #    - ------
    #    0      H 
    return 0.5*(sqrt(H**2+alpha**2)-H)
  
  def heavyside_approx(H, alpha):
    # A smooth approximation to the heaviside function
    return 0.5*(H/(sqrt(H**2+alpha**2)))+0.5 

  def norm_approx(u, alpha):
    # A smooth approximation to ||u||
    return sqrt(inner(u, u)+alpha**2)

  max_depth = 5.
  h_static = (1.-triangle.x[0]/basin_x)*max_depth 

  def H_tilde(eta):
    return h_static + wd_f(h_static + eta, alpha) + eta

  t = 0.
  g = 9.81
  c0 = 0.0025
  eta_bc = Expression(("elevation*sin(2*pi*t/period)"), elevation = 2.0, t = 0, period = 12*60*60) 

  # DIRK22 Runge Kutta method
  a11 = (2.-sqrt(2.))/2.
  a21 = 1.-(2.-sqrt(2.))/2.
  a22 = (2.-sqrt(2.))/2.
  b1 = 1.-(2.-sqrt(2.))/2.
  b2 = (2.-sqrt(2.))/2.

  def continuity(a, u, eta, eta_test):
    return (- a * inner(grad(eta_test), H_tilde(eta)*u)*dx
            + a * eta_test * H_tilde(eta) * inner(u, n)*ds(1) 
           )

  def momentum(a, u, u_test, eta):
    return (a * inner(grad(u)*u, u_test)*dx    
            + a * g*inner(grad(eta), u_test)*dx 
            - a * (eta-eta_bc) * g * inner(u_test, n)*ds(1)
            #+ a * c0 * inner(sqrt(inner(u,u)) * u, u_test)*(H_tilde(eta)**(-4./3))*dx
            + a * c0 * norm_approx(u, alpha) * inner(u, u_test)*(H_tilde(eta)**(-4./3))*dx
           )

  F1 = (inner((u_1 - u_old)/dlt, u_test)*dx 
        + momentum(a11, u_1, u_test, eta_1) 
        + inner((H_tilde(eta_1) - H_tilde(eta_old))/dlt, eta_test)*dx 
        + continuity(a11, u_1, eta_1, eta_test)
       )

  F2 = (inner((u_2 - u_old)/dlt, u_test)*dx
        + momentum(a21, u_1, u_test, eta_1) 
        + momentum(a22, u_2, u_test, eta_2) 
        + inner((H_tilde(eta_2) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(a21, u_1, eta_1, eta_test)
        + continuity(a22, u_2, eta_2, eta_test)
       )

  F3 = (inner((u_new - u_old)/dlt, u_test)*dx
        + momentum(b1, u_1, u_test, eta_1) 
        + momentum(b2, u_2, u_test, eta_2) 
        + inner((H_tilde(eta_new) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(b1, u_1, eta_1, eta_test)
        + continuity(b2, u_2, eta_2, eta_test)
       )

  ########################## Step 1: Run the forward model with the optimal parameters and record the wetting and drying front #####################
  if output:
    store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
  adjointer.time.start(t)

  # This array array will contain the boundary functions of all timesteps
  eta_bc_proj = [] 
  # Keep also a copy of these optimal controls for later comparison
  eta_bc_proj_opt = [] 
  # Record the initial wetting and drying front
  wd_obs_tmp = project(heavyside_approx(eta_new - h_static, alpha), Pdg, annotate = False)
  wd_obs = project(wd_obs_tmp, Pdg)

  while t < T:
    if MPI.process_number() == 0:
      print "Computing time t=%f h" % (float(t)/60/60)
    eta_bc.t = t + dlt # TODO: Which timelevel do we actually need here?
    eta_bc_proj.append(project(eta_bc, R))
    eta_bc_proj_opt.append(project(eta_bc, R))

    solve(replace(F1, {eta_bc: eta_bc_proj[-1]}) == 0, z_1)
    solve(replace(F2, {eta_bc: eta_bc_proj[-1]}) == 0, z_2)
    solve(replace(F3, {eta_bc: eta_bc_proj[-1]}) == 0, z_new)
    z_old.assign(z_new)
    # Record the wetting and drying front
    wd_obs_tmp = project(heavyside_approx(eta_new - h_static, alpha), Pdg, annotate = False)
    wd_obs.assign(wd_obs_tmp)

    t += dlt
    if output:
      store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
    adj_inc_timestep(time=t, finished = t>=T)

  ########################## Step 2: Define the functional using the recorded wetting and drying front  #####################
  J = Functional(0.5*inner(heavyside_approx(eta_new - h_static, alpha) - wd_obs, heavyside_approx(eta_new - h_static, alpha) - wd_obs)*dx*dt) 
  reduced_functional = ReducedFunctional(J, InitialConditionParameter(eta_bc_proj[0]))

  # Evaluating the functional will yield a value that is close to zero. It will not be exactly zero because we projected the observations onto the Pdg field.
  # Therefore the error can be reduced by increasing the resolution/degree of the Pdg function space
  func_value = reduced_functional(eta_bc_proj[0])
  print "Reduced functional: ", func_value
  if abs(func_value)>1.e-4:
    print "Functional value exceeds tolerance at optimal control"
  # Similarily, the derivative should be close to zero ...
  func_deriv = norm(compute_gradient(J, InitialConditionParameter(eta_bc_proj[0]), forget = False))
  print "Functional derivative: ", func_deriv
  if func_deriv>1.e-4:
    print "Norm of functional derivative exceeds tolerance at optimal control"


  ########################## Step 3: Run the taylor test  #####################
  conv = taylor_test(reduced_functional, InitialConditionParameter(eta_bc_proj[0]), reduced_functional(eta_bc_proj[0]), compute_gradient(J, InitialConditionParameter(eta_bc_proj[0]), forget = False), seed = 1e-2)
  if conv<1.9:
    import sys
    info_red("Taylor remainder test failed")
    sys.exit(1)
  else:
    info_green("Taylor remainder test passed")

if __name__ == "__main__":
  compute_solution(T = 3*60, dlt = 60)
