#!/usr/bin/python
import sys
import re
import matplotlib.pyplot as plt
from matplotlib import rc
filename = sys.argv[1]
f = open(filename, "r")

# Read the output file of the optimisation run and parse it
# This script assumes that scipy.l_bfgs_b was used.

# The iteration numbers
it = []
# The functional values
func = []
tol_reached = False
for line in f:
    if "At iterate" in line:
        m = re.match(r"At iterate\s+([0-9]+)", line)
        it.append(m.group(1))
        m = re.match(r".*f=\s*([0-9|\.|D\+\-]+)", line)
        try:
            func.append(float(m.group(1).replace("D", "e")))
        except Exception as e:
            print "Error in line ", line
            raise e

f.close()
print "Iterations :", it
print "Functional values :", func 

# The produce a nice plot
filename = "iter_plot.pdf"
scaling = 0.7
rc('text', usetex = True)
plt.figure(1, figsize = (scaling*7., scaling*4.))
plt.gcf().subplots_adjust(bottom=0.15)
plt.plot(it, func, color = 'black')
plt.yscale('log')
#plt.axis([0, times[-1], -2.5, 2.5])
#plt.xticks(numpy.arange(0, times[-1]+1, 5))
#plt.yticks(numpy.arange(14, basin_x_total/1000, 2))
plt.ylabel(r"$J\left(\eta_D\right)$")
plt.xlabel(r"Iteration")
plt.savefig(filename)
plt.close()
