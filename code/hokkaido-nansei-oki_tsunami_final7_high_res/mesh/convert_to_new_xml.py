#!/usr/bin/python
import os
import sys
from dolfin import *
base = os.path.splitext(sys.argv[1])[0] 
mesh = Mesh(base + '.xml')
mesh_function = MeshFunction('size_t', mesh, base + "_facet_region.xml")
file = File(base + '_facet_region.xml')
file << mesh_function
