from dolfin import *
from grd_reader import NetCDFExpression

basename = "MonaiValley_C"
mesh = Mesh("mesh/" + basename + ".xml")

cg2 = FiniteElement("CG", triangle, 2)
P = FunctionSpace(mesh, cg2)

h_static = -NetCDFExpression(filename="raw_data/Bathymetry.grd", field="z", degree=2)
h_static = project(-h_static, P)

eps_sq = project(CellSize(mesh)**2*inner(grad(h_static), grad(h_static)), P)
eps_sq_max = max(eps_sq.vector().array())
print "Estimated epsilon = ", eps_sq_max**0.5
