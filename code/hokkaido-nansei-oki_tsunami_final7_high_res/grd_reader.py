#!/usr/bin/python
from dolfin import *
from numpy import isnan, cos, sin, loadtxt, array, nan, linspace, meshgrid
from math import floor
import matplotlib.pyplot as plt

class NetCDFReader(object):
  def __init__(self, filename):
    from scipy.io.netcdf import NetCDFFile
    self.nc = NetCDFFile(filename)
    y = self.nc.variables['y'][:]
    self.delta_y = y[1]-y[0]
    x = self.nc.variables['x'][:]
    self.delta_x = x[1]-x[0]
    self.x = x
    self.y = y

  def set_field(self,field):
    self.val = self.nc.variables[field]

  def __call__(self, x, y):
    i = int(floor((x-self.x[0])/self.delta_x))
    beta = (x-self.x[0])/self.delta_x - float(i)
    j = int(floor((y-self.y[0])/self.delta_y))
    alpha = (y-self.y[0])/self.delta_y - float(j)
    try:
      if j+1 == len(self.y):
        o = 0
      else:
        o = 1
      if i+1 == len(self.x):
        q = 0
      else:
        q = 1

      value = ((1.0-beta)*((1.0-alpha)*self.val[j,i]+alpha*self.val[j+o,i])+
        beta*((1.0-alpha)*self.val[j,i+q]+alpha*self.val[j+o,i+q]))
    except:
      print x, y, i, j
      raise
    if isnan(value):
      return 0.0
    else:
      return value

  def plot(self):
    xpl = linspace(min(self.x), max(self.x), num = 100)
    ypl = linspace(min(self.y), max(self.y), num = 100)
    xpl, ypl = meshgrid(xpl, ypl)
    z = [[self(p1,p2) for p1, p2 in zip(p1row, p2row)]
                                           for p1row, p2row in zip(xpl, ypl)]

    cs = plt.contourf(xpl, ypl, z)
    plt.show()

class NetCDFReaderExpression(Expression):
  """Dolfin Expression initialised from a NetCDFReader object.
  Needs evaluating on a mesh with x,y coordinates."""
  def __init__(self,nc):
    self.nc = nc

  def eval(self, values, x):
    values[0] = self.nc(x[0], x[1])

class NetCDFExpression(Expression):
  """Dolfin Expression initialised from field in a NetCDF file.
  Needs evaluating on a mesh with x,y coordinates.
  
  Arguments:
    - filename: A string containing the filename
    - field: A string containing the field name
  """
  def __init__(self, **kwargs):
    self.nc = NetCDFReader(kwargs["filename"])
    self.nc.set_field(kwargs["field"])

  def eval(self, values, x):
    values[0] = self.nc(x[0], x[1])

if __name__ == "__main__":
    print "Reading data"
    bath = NetCDFReader('raw_data/Bathymetry.grd')
    bath.set_field("z")
    print "Plotting data"
    bath.plot()
