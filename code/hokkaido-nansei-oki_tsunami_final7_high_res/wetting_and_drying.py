''' This example implements the shallow water equations with wetting and drying algorithm based on:
  Kaernae et al., A fully implicity wetting-drying method for DG-FEM shallow water models, with an application to the Schledt Estuary, 2011, Comput. Methods Appl. Mech. Engrg.
  '''
from dolfin import *
from dolfin_adjoint import *
import inputwave
from grd_reader import NetCDFExpression
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy

# Global settings
parameters["std_out_all_processes"] = False
dolfin.parameters['form_compiler']['cpp_optimize'] = True
dolfin.parameters['form_compiler']['cpp_optimize_flags'] = '-O3 -ffast-math -march=native'
dolfin.parameters['form_compiler']['optimize'] = True
#dolfin.parameters["optimisation"]["test_gradient"] = True
parameters["form_compiler"]["quadrature_degree"] = 20
parameters["form_compiler"]["representation"] = "uflacs"
parameters["ghost_mode"] = 'shared_facet'
set_log_level(ERROR)

# Smoothing value
alpha = Constant(0.1)
offset = 4.

def probe(point, f):
    """ Parallel safe point evaluation """
    try:
	uu = f(point)
    except:
	uu = -1e16
    return MPI.max(mpi_comm_world(), uu)

def compute_solution(T = 24, dlt = 0.5, output=True):
  ########################## Step 0: Define the equations and do the setup work #####################

  basename = "MonaiValley_C"
  mesh = Mesh("mesh/" + basename + ".xml")
  n = FacetNormal(mesh)
  surf_ids = MeshFunction("size_t", mesh, "mesh/" + basename + "_facet_region.xml")
  ds = Measure("ds")(subdomain_data=surf_ids)

  # The functions play following role:
  # eta: free surface elevation versus a reference level
  # h: static bathymetry
  # H: eta + h
  # \tilde h: h + f(H), where f is an approximation to the function: x -> max(|-x|, 0)
  # \tilde H: H = eta + \tilde h > 0!
  dg1 = VectorElement("DG", triangle, 1)
  cg2 = FiniteElement("CG", triangle, 2)
  dg1cg2 = MixedElement([dg1, cg2])

  P = FunctionSpace(mesh, cg2)
  Pdg = FunctionSpace(mesh, "DG", 6)
  R = FunctionSpace(mesh, "R", 0)
  Z = FunctionSpace(mesh, dg1cg2)

  if output:
    u_pvd = File("results_dg/u.pvd")
    eta_pvd = File("results_dg/eta.pvd")
    h_static_pvd = File("results_dg/h_static.pvd")
    h_tilde_pvd = File("results_dg/h_tilde.pvd")
    surface_elevation_pvd = File("results_dg/surface_elevation.pvd")
    wet_dry_pvd = File("results_dg/wet_dry.pvd")
    smooth_wet_dry_pvd = File("results_dg/wd_smooth.pvd")
    obs_pvd = File("results_dg/observations.pvd")

  def store(z, h_static, h_tilde, t):
    t=float(t)
    (u, eta) = z.split(deepcopy=True)
    u_pvd << (u, t)
    eta_pvd << (eta, t)
    h_static_pvd << (project(h_static, P, annotate = False), t)
    h_tilde_pvd << (project(h_tilde, P, annotate = False), t)
    surface_elevation_pvd << (project(conditional(gt(eta, -h_static), eta, -h_static), Pdg, annotate = False), t)
    wet_dry_pvd << (project(conditional(gt(eta, -h_static), 1., 0.), Pdg, annotate = False), t)
    smooth_wet_dry_pvd << (project(heavyside_approx(- eta - h_static, alpha), Pdg, annotate = False), t)

  z_1 = Function(Z, name = "z1")
  (u_1, eta_1) = split(z_1)
  z_2 = Function(Z, name = "z2")
  (u_2, eta_2) = split(z_2)
  z_new = Function(Z, name = "z_new")
  (u_new, eta_new) = split(z_new)
  z_old = Function(Z, name = "z_old")
  (u_old, eta_old) = split(z_old)
  z_test = TestFunction(Z)
  (u_test, eta_test) = split(z_test)
  wd_obs = Function(Pdg, name = "wd_obs")

  def wd_f(H, alpha):
    # An smooth approximation to max(-H, 0)
    # f(H)
    # \
    #  \
    #   \
    #    - ------
    #    0      H
    return 0.5*(sqrt(H**2+alpha**2)-H)

  def heavyside_approx(H, alpha):
    # A smooth approximation to the heaviside function
    return 0.5*(H/(sqrt(H**2+alpha**2)))+0.5

  def norm_approx(u, alpha):
    # A smooth approximation to ||u||
    return sqrt(inner(u, u)+alpha**2)

  max_depth = 5.
  h_static = -NetCDFExpression(filename="raw_data/Bathymetry.grd", field="z", degree=2)
  #h_static = project(-bathymetry, P)
  #plot(h_static, interactive = True)

  # Helper for eps computation
  #plot(grad(project(h_static, P))) # approx [-0.8, 0.8]
  #mesh.hmin() # approx 0.05
  #mesh.hmax() # approx 0.7
  # => eps \in [0..0.5]

  def H_tilde(eta):
    return h_static + wd_f(h_static + eta, alpha) + eta

  t = 0.
  g = 9.81
  c0 = 0.025
  class InputWaveExpression(Expression):
    def __init__(self, **kwargs):
      self.t = kwargs["t"]

    def eval(self, value, x):
          value[0] = inputwave.val(x, self.t)

  eta_bc = InputWaveExpression(t=t, degree=2)

  # DIRK22 Runge Kutta method
  a11 = (2.-sqrt(2.))/2.
  a21 = 1.-(2.-sqrt(2.))/2.
  a22 = (2.-sqrt(2.))/2.
  b1 = 1.-(2.-sqrt(2.))/2.
  b2 = (2.-sqrt(2.))/2.
  c1 = (2.-sqrt(2.))/2.
  c2 = 1.

  def continuity(a, u, eta, eta_test):
    return (- a * inner(grad(eta_test), H_tilde(eta)*u)*dx
            + a * eta_test * H_tilde(eta) * inner(u, n)*ds(1)
           )

  def momentum(a, u, u_test, eta):
    u_down = (dot(u, n)-norm_approx(dot(u, n), alpha))/2.0
    return (#a * inner(grad(u)*u, u_test)*dx
            a * dot(dot(u, nabla_grad(u)), u_test)*dx
            - a * dot(u_down('+')*jump(u), u_test('+'))*dS
            + a * dot(u_down('-')*jump(u), u_test('-'))*dS
            + a * g*inner(grad(eta), u_test)*dx
            - a * (eta-eta_bc) * g * inner(u_test, n)*ds(1)
            #+ a * g * c0**2 * inner(sqrt(inner(u,u)) * u, u_test)*(H_tilde(eta)**(-4./3))*dx
            + a * g * c0**2 * norm_approx(u, alpha) * inner(u, u_test)*(H_tilde(eta)**(-4./3))*dx
           )

  F1 = (inner((u_1 - u_old)/dlt, u_test)*dx
        + momentum(a11, u_1, u_test, eta_1)
        + inner((H_tilde(eta_1) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(a11, u_1, eta_1, eta_test)
       )

  F2 = (inner((u_2 - u_old)/dlt, u_test)*dx
        + momentum(a21, u_1, u_test, eta_1)
        + momentum(a22, u_2, u_test, eta_2)
        + inner((H_tilde(eta_2) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(a21, u_1, eta_1, eta_test)
        + continuity(a22, u_2, eta_2, eta_test)
       )

  F3 = (inner((u_new - u_old)/dlt, u_test)*dx
        + momentum(b1, u_1, u_test, eta_1)
        + momentum(b2, u_2, u_test, eta_2)
        + inner((H_tilde(eta_new) - H_tilde(eta_old))/dlt, eta_test)*dx
        + continuity(b1, u_1, eta_1, eta_test)
        + continuity(b2, u_2, eta_2, eta_test)
       )

  ########################## Step 1: Run the forward model with the optimal parameters and record the wetting and drying front #####################
  if output:
    store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
  adjointer.time.start(t)

  # The following arrays will contain the boundary functions of all timesteps and the upper and lower bounds for the control problem
  # All right, so in order to make this work we need some control fields
  # We create a list of functions each of which describes the boundary value
  # at a certain timestep (we can not reuse one function since timestep boundary value is independently controlled)
  eta_bc_proj = [project(eta_bc, R)]
  eta_bc_ub = [project(Constant(0.02), R)]
  eta_bc_lb = [project(Constant(-0.02), R)]
  # Keep also a copy of these optimal controls for later comparison
  eta_bc_proj_opt = [project(eta_bc, R)]
  # Record the initial wetting and drying front
  wd_obs_tmp = project(heavyside_approx(- eta_new - h_static, alpha), Pdg, annotate = False)
  wd_obs = project(wd_obs_tmp, Pdg)
  # Record the simulated wetting and drying front
  wd = project(heavyside_approx(- eta_new - h_static, alpha), Pdg)
  times = [t]

  while t < T:
    if MPI.rank(mpi_comm_world()) == 0:
      print "Computing time t=%f h" % (float(t)/60/60)
    # Fix the boundary control in the offset hours
    if t >= offset and t < T - offset:
      eta_bc.t = t + dlt - offset
    eta_bc_proj.append(project(eta_bc, R))
    eta_bc_ub.append(project(Constant(0.02), R))
    eta_bc_lb.append(project(Constant(-0.015), R))
    eta_bc_proj_opt.append(project(eta_bc, R))

    # Update the boundary values and solve the first Runge-Kutta step
    solve(replace(F1, {eta_bc: (1.-c1)*eta_bc_proj[-2] + c1*eta_bc_proj[-1]}) == 0, z_1)

    # Update the boundary values and solve the second Runge-Kutta step
    solve(replace(F2, {eta_bc: eta_bc_proj[-1]}) == 0, z_2)

    # Save the optimal control values and solve the final Runge-Kutta step
    solve(replace(F3, {eta_bc: eta_bc_proj[-1]}) == 0, z_new)
    z_old.assign(z_new)

    # Record the wetting and drying front
    wd_obs_tmp = project(heavyside_approx(- eta_new - h_static, alpha), Pdg, annotate = False)
    obs_pvd << (wd_obs_tmp, t)
    wd_obs.assign(wd_obs_tmp)
    # Record the simulated wetting and drying front
    wd_tmp = project(heavyside_approx(- eta_new - h_static, alpha), Pdg)
    wd.assign(wd_tmp)

    t += dlt
    times.append(t)
    if output:
      store(z_new, h_static, H_tilde(eta_new) - eta_new, t)
    adj_inc_timestep(time=t, finished = t>=T)

  ########################## Step 2: Define the functional using the recorded wetting and drying front  #####################
  form = 0.5*inner(wd - wd_obs, wd - wd_obs)*dx*dt
  scale = Constant(1)
  J = Functional(scale*form)

  def control_plot(times, controls, filename, ylabel = r"$\eta_D$ [cm]", subtract_optimal_controls = False):
    ''' Saves a plot of the controls '''
    if subtract_optimal_controls:
      Y = [probe((0, 0), control)-probe((0, 0), opt_control) for control, opt_control in zip(controls, eta_bc_proj_opt[:-int(2./dlt)])]
    else:
      Y = [probe((0, 0), control) for control in controls]

    if MPI.rank(mpi_comm_world()) == 0:
      scaling = 0.7
      rc('text', usetex = True)
      plt.figure(1, figsize = (scaling*7., scaling*4.))
      plt.gcf().subplots_adjust(bottom=0.15)
      plt.gcf().subplots_adjust(left=0.3)
      # Convert m to cm
      Y = 100. * numpy.array(Y)
      # Plot
      plt.plot(times[:-int(2./dlt)], Y, color = 'black')
      plt.xlim([0, times[-1]])
      #plt.xticks(numpy.arange(0, times[-1]+1, 5))
      plt.ylabel(ylabel)
      plt.xlabel(r"Time [s]")
      print "Saving control to ", filename
      plt.savefig(filename)
      plt.close()

  # Save a plot of the optimal control values
  control_plot(times, eta_bc_proj_opt[:-int(2./dlt)], "controls/controls_optimal.pdf", ylabel = r"$\eta_D^{\textrm{exact}}$ [cm]")
  # And create a callback that saves the control values at every optimisation iteration
  eval_counter = [0]

  def eval_callback(value):
    eval_counter[0] += 1
    control_plot(times, value, "controls/controls_"+str(eval_counter[0])+".pdf")
    control_plot(times, value, "controls/controls_final.pdf")
    control_plot(times, value, "controls/controls_errors_"+str(eval_counter[0])+".pdf", subtract_optimal_controls=True, ylabel = r"$\eta_D - \eta_D^{\textrm{exact}}$ [cm]")
    max_err = max([abs(probe((0, 0), control)-probe((0, 0), opt_control)) for control, opt_control in zip(value, eta_bc_proj_opt[:-int(2./dlt)])])
    print "Maximal control value error: ", max_err, "m"
    if max_err < 0.0001:
      print "********************************** Control is 0.0001m close to the solution found after %i iterations ************************" % eval_counter[0]

  ########################## Step 3: Forget the optimal parameters and recover them using the optimisation  #####################
  for p in eta_bc_proj[:-int(2./dlt)]:
    p.vector().zero()

  ctrl_values = eta_bc_proj[:-int(2./dlt)]
  controls = [Control(p) for p in ctrl_values]
  rf = ReducedFunctional(J, controls, eval_cb_pre=eval_callback)
  scale.assign(1./rf(ctrl_values)) # Normalize functional
  
  minimize(rf, bounds = [eta_bc_lb[:-int(2./dlt)], eta_bc_ub[:-int(2./dlt)]],
      options={'gtol':  1e-200, 'ftol': 1e-9, 'maxfun': 1000})

  ########################## Step 4: Now compare how well we reconstructed the controls  #####################
  errors = [assemble(inner(p_opt-p, p_opt-p)*ds(1)) for (p, p_opt) in zip(eta_bc_proj_opt, eta_bc_proj)]
  for (timestep, error) in zip(range(len(errors)), errors):
    print "Timestep: %i   Error: %f"% (timestep, error)
  print "Total error: ", sum(errors)

if __name__ == "__main__":
  compute_solution( T = 24 + 2*offset)
