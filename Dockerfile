# Builds a Docker image for reproducing the results in the wetting and drying
# adjoint paper by Funke et.al

FROM quay.io/dolfinadjoint/dev-dolfin-adjoint:latest
MAINTAINER Simon Funke <simon@simula.no>

USER root
RUN sudo apt-get update && sudo apt-get -y install expect texlive texlive-latex-extra dvipng

USER fenics
RUN mkdir -p ~/.config/matplotlib && echo "backend: agg" >  ~/.config/matplotlib/matplotlibrc
RUN git clone https://simon_funke@bitbucket.org/simon_funke/wetting_and_drying_optimisation_code.git

USER root
